%% calc_eps_SF2.m
% Calculate the dissipation rate at all depths using a second order
% structure function.
%
% August 9, 2016 - copied here from SF paper dir and modified to compute
%                  eps at all possible depths

clear
mname = mfilename('fullpath');

% sites = {'GP130730TA','GP120904TA','GP130620BPb','GP130620BPa'};
sites = {'GP130620BPb'};
epsoptions.Const = 2.0;
epsoptions.figure = 0;
epsoptions.method = 'allR';
epsoptions.nrmax = 9;
epsoptions.bin_min = 3; % bin above bottom to start at

% fields = {'v2','v3'};
fields = {'v1','v2','v3','v4'};
epsoptions.sigmaN_v = 0.0466; % Noise normalization


for ss = 1:length(sites)
    site = sites{ss};
    
    
    
    switch site
        case 'GP130730TA'
            fileinfo.datadir = '/home/justine/Research/data/turbulence2013/ADCPs/';
            fileinfo.fileroot = 'GP130730TA_beam';
            
            fileinfo.savefile = '../../output/GP130730TA_epsSF2';
            epsoptions.avgmethod = 'Burst';
            epsoptions.bin_max = 65;
            numfiles = 1;
            flg.concatenate = 0;
        case 'GP130620BPa'
            fileinfo.datadir = '/home/justine/Research/data/ecoEII/GrandPassage/';
            fileinfo.fileroot = 'BPa_beam';
            fileinfo.outdir = '../../output/';
            fileinfo.savefile = '../../output/GP130620BPa_epsSF2';
            numfiles = 3;
            epsoptions.avgmethod = 'Ens';
            epsoptions.bin_max = 65;
            flg.concatenate = 1;
        case 'GP130620BPb' % THIS ONE
            fileinfo.datadir = '../../../Data/GP130620BPb/RawData/';
            fileinfo.fileroot = 'BPb_beam';
            fileinfo.savefile = '../output/GP130620BPb_epsSF2_all';
            fileinfo.outdir = '../output/';
            numfiles = 1; % There were 3, but just use one for ATOMIX
            epsoptions.avgmethod = 'Ens';
            epsoptions.bin_max = 60;
            flg.concatenate = 1;
        case 'GP120904TA'
            fileinfo.datadir = '/home/justine/Research/data/turbulence2012/ADCP/';
            fileinfo.fileroot = 'GP120904TA_beam';
            fileinfo.savefile = '../../output/GP120904TA_epsSF2';
            numfiles = 1;
            epsoptions.avgmethod = 'Ens';
            flg.concatenate = 0;
            epsoptions.bin_max = 56;
    end
    
    % options
    if strcmp(epsoptions.avgmethod, 'Burst')
        min_dt = 5; % minimum time between bursts
    else
        t_ens = 5;
    end
    
    %**** End of optional inputs****
    
    
    %% Load data
    for ww = 1:numfiles
        if strcmp(site,'GP130730TA') ||  strcmp(site,'GP120904TA')
            fileinfo.fileend = '';
        else
            fileinfo.fileend = ['_wk',num2str(ww)];
        end
        for bb = 1:length(fields)
            vb = fields{bb};
            file = [fileinfo.datadir,fileinfo.fileroot,vb(2),fileinfo.fileend];
            disp(file)
            a = load(file);
            
            if bb == 1
                %             adcp.mtime = [adcp.mtime a.adcp.mtime];
                adcp.mtime = a.adcp.mtime;
                mtime1 = a.adcp.mtime;
                z = a.adcp.hab;
            else
                if ~isequal(mtime1, a.adcp.mtime)
                    error('Different time vectors')
                end
            end
            %         adcp.(vb) = [adcp.(vb) a.adcp.(vb)];
            adcp.(vb) = a.adcp.(vb);
        end
        
        
        %% identify bursts
        if strcmp(epsoptions.avgmethod,'Burst')
            [ind_start, ind_end] = get_burst_inds(adcp.mtime,min_dt/60/24);
            nburst = length(ind_start);
        elseif strcmp(epsoptions.avgmethod,'Ens')
            % identify ensembles
            [ind_start, ind_end] = get_ens_inds(adcp.mtime*24*60,t_ens,1.5*60);
            nburst = length(ind_start);
        end
        
        %% Initialize
        z_bins = z(1:epsoptions.bin_max);
        nz = length(z_bins);
        yd = NaN*ones(1,nburst);
        for ff = 1:length(fields)
            eps.(fields{ff}) = NaN*ones(nz,nburst);
            sigmaN.(fields{ff}) = NaN*ones(nz,nburst);
            R2.(fields{ff}) = NaN*ones(nz,nburst);
            
            D.(fields{ff}) = cell(nz,nburst);   
            r = cell(nz,nburst);  
            
            Rinfo.(fields{ff}) = cell(nz,nburst);
                                       
            
        end
              
        
        %% cycle and calculate epsilon
        for nn = 1:nburst
            inds_t = ind_start(nn):ind_end(nn);
            npts = length(inds_t);
            
            yd(nn) = get_yd(mean(adcp.mtime(inds_t)));
            
            % reference speed (for plotting only)
            epsoptions.Uref = NaN;
            
            
            for ff = 1:length(fields)
                % Get primed velocity
                v = adcp.(fields{ff})(:,inds_t);
                %                  vm = nanmean(v,2); % mean
                %                  vp2 = (v - vm*ones(1,npts));
                vp = detrend(v')'; % fluctuation (i.e. remove mean and trend)
                
%                 if ff == 4 && nn == 5
%                     return
%                 end
                
                % cycle through z and compute dissipation rate
                for zz = 1:nz
                    if (zz < epsoptions.bin_min) || (zz > epsoptions.bin_max - epsoptions.bin_min+1)
                        continue
                    end
                    ind_z = zz;
                    z_c = z(ind_z);
                    
                    %% Get data over averaging region
                    nrmax = min(2*ind_z-1,epsoptions.nrmax);
                    nz_R0 = floor(nrmax/2); % number of bins from centre
                    inds_zavg = ind_z-nz_R0:ind_z+nz_R0;
                    %                     z = a.adcp.hab(inds_zavg);
                    %                     nz = length(inds_zavg);
                    
                    switch epsoptions.method
                        case 'allR'
                            
                            epsoptions.figure = 0;
                            epsoptions.order = 2;
                            options = rmfield(epsoptions,'nrmax');
                            [eps_tmp,sigN_tmp,R2_tmp,r_tmp,D_tmp,Rinfo_tmp] = ...
                                calc_eps_ADCP_SF_allR(vp(inds_zavg,:)',z(inds_zavg),options);
                        otherwise
                            error('not set up for this method')
                    end
                    
                    eps.(fields{ff})(zz,nn) = real(eps_tmp);
                    sigmaN.(fields{ff})(zz,nn) = sigN_tmp;
                    R2.(fields{ff})(zz,nn) = R2_tmp;
                    D.(fields{ff}){zz,nn} = D_tmp;
                    Rinfo.(fields{ff}){zz,nn} = struct2cell(Rinfo_tmp);
                    
                    if ff == 1;
                     r{zz,nn} = r_tmp;
                    end                 
                    
                    if ff == 1 && zz == epsoptions.bin_min && nn == 1
                        Rinfo.names = fieldnames(Rinfo_tmp);
                    end
                    
                end%zz
                
               
                
            end%ff
            disp_percdone(nn,nburst,2)
        end%nn
        
%         %% concatenate
%         yd = [yd; yd_subset];
%         for ff = 1:length(fields)
%             eps.(fields{ff}) = [eps.(fields{ff}); eps_subset.(fields{ff})];
%             sigmaN.(fields{ff}) = [sigmaN.(fields{ff}); sigmaN_subset.(fields{ff})];
%             R2.(fields{ff}) = [R2.(fields{ff}); R2_subset.(fields{ff})];
%             D.(fields{ff}) = [D.(fields{ff}); D_subset.(fields{ff})];
%             
%             Rinfo.(fields{ff}) = catstructs_JMM(Rinfo.(fields{ff}),Rinfo_subset.(fields{ff}),1);
%         end
        
        
        %% save data
        metadata = get_metadata(mname);
        z = z_bins;
        save([fileinfo.savefile,fileinfo.fileend],'yd','z','eps','sigmaN','R2','D','r','Rinfo','epsoptions','metadata')
    
        clear adcp
    end
    
    if flg.concatenate
        d = dir([fileinfo.savefile,fileinfo.fileend(1:2),'*']);
        for dd = 1:length(d)
                

                if dd == 1
                    load([fileinfo.outdir d(dd).name]);                    
                else
                    data_add = load([fileinfo.outdir d(dd).name]);
                    yd = [yd data_add.yd];
                    eps = catstructs_JMM(eps,data_add.eps,2);
                    sigmaN = catstructs_JMM(sigmaN,data_add.sigmaN,2);
                    R2 = catstructs_JMM(R2,data_add.R2,2);
                    D = catstructs_JMM(D,data_add.D,2);
                    r = [r data_add.r];
                    names = Rinfo.names;
                    Rinfo = rmfield(Rinfo,'names');
                    Rinfo = catstructs_JMM(Rinfo,data_add.Rinfo);
                    Rinfo.names = names;
                    
                end
        end
        save([fileinfo.savefile],'yd','z','eps','sigmaN','R2','D','r','Rinfo','epsoptions','metadata')
    end
        
    
    
end