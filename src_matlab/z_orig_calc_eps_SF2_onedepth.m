%% calc_eps_SF2_onedepth.m
% Calculate the dissipation rate at one depth using a second order 
% structure function. 
%
% March 4, 2016
% June 6, 2014 - modified to only do fit at Z = 10.11 level

clear
mname = mfilename('fullpath');
flg.savemat = 1;

%sites = {'GP130730TA','GP120904TA','GP130620BPb','GP130620BPa'};
sites = {'GP130620BPb'};
epsoptions.Z = 10.11;
epsoptions.Const = 2.0;
epsoptions.figure = 0;
%epsoptions.method = 'cdiff';
epsoptions.method = 'allR';
epsoptions.nrmax = 9;
% fields = {'v3'};
fields = {'v1','v2','v3','v4'};
epsoptions.sigmaN_v = 0.0466; % Noise normalization


for ss = 1:length(sites)
    site = sites{ss};
    
    
    
    switch site
        case 'GP120904TA'
            fileinfo.datadir = '/home/justine/Research/data/turbulence2012/ADCP/';
            fileinfo.fileroot = 'GP120904TA_beam';
            fileinfo.savefile = '../output/GP120904TA_epsSF2.mat';
            numfiles = 1;
            epsoptions.avgmethod = 'Ens';
        case 'GP130620BPa'
            fileinfo.datadir = '/home/justine/Research/data/ecoEII/GrandPassage/';
            fileinfo.fileroot = 'BPa_beam';
            fileinfo.savefile = '../output/GP130620BPa_epsSF2.mat';
            numfiles = 3;
            epsoptions.avgmethod = 'Ens';
        case 'GP130620BPb' %THIS ONE
            fileinfo.datadir = '../../../Data/GP130620BPb/RawData/';
            fileinfo.fileroot = 'BPb_beam';
            fileinfo.savefile = '../output/GP130620BPb_epsSF2_10m.mat';
            numfiles = 1;
            epsoptions.avgmethod = 'Ens';
        case 'GP130730TA'
            fileinfo.datadir = '/home/justine/Research/data/turbulence2013/ADCPs/';
            fileinfo.fileroot = 'GP130730TA_beam';
            fileinfo.savefile = '../output/GP130730TA_epsSF2';
       %     fileinfo.noise = '/home/justine/Research/work/turbulence2013/ADCPs/output/GP130730TA_noisefloor.mat'
            epsoptions.avgmethod = 'Burst';
            numfiles = 1;
    end
    
    % options
    if strcmp(epsoptions.avgmethod, 'Burst')
        min_dt = 5; % minimum time between bursts
    else
        t_ens = 5;
    end
    
    %**** End of optional inputs****
    
    %% Initialize
    adcp.mtime = [];
    for ff = 1:length(fields)
        adcp.(fields{ff}) = [];
        yd = [];
        eps.(fields{ff}) = [];
        sigmaN.(fields{ff}) = [];
        R2.(fields{ff}) = [];
        D.(fields{ff}) = [];
        Rinfo.(fields{ff}).slope = [];
        Rinfo.(fields{ff}).yint = [];
        Rinfo.(fields{ff}).CI_slope = [];
        Rinfo.(fields{ff}).CI_yint = [];
        
    end

    
    %% Load data
    for ww = 1:numfiles
        if strcmp(site,'GP130730TA') ||  strcmp(site,'GP120904TA')
            fileinfo.fileend = '';
        else
            fileinfo.fileend = ['_wk',num2str(ww)];
        end
        for bb = 1:length(fields)
            vb = fields{bb};
            file = [fileinfo.datadir,fileinfo.fileroot,vb(2),fileinfo.fileend];
            disp(file)
            a = load(file);
            
            if bb == 1
                %             adcp.mtime = [adcp.mtime a.adcp.mtime];
                adcp.mtime = a.adcp.mtime;
                mtime1 = a.adcp.mtime;
            else
                if ~isequal(mtime1, a.adcp.mtime)
                    error('Different time vectors')
                end
            end
            %         adcp.(vb) = [adcp.(vb) a.adcp.(vb)];
            adcp.(vb) = a.adcp.(vb);
            
            
        end

        %% Get data over averaging region
        ind_z = get_nearestindex(a.adcp.hab,epsoptions.Z);
        nz_R0 = floor(epsoptions.nrmax/2); % number of bins from centre
        inds_zavg = ind_z-nz_R0:ind_z+nz_R0;
        z = a.adcp.hab(inds_zavg);
        nz = length(inds_zavg);

      
        
        %% identify bursts
        if strcmp(epsoptions.avgmethod,'Burst')
            [ind_start, ind_end] = get_burst_inds(adcp.mtime,min_dt/60/24);
            nburst = length(ind_start);
        elseif strcmp(epsoptions.avgmethod,'Ens')
            % identify ensembles
            [ind_start, ind_end] = get_ens_inds(adcp.mtime*24*60,t_ens,1.5*60);
            nburst = length(ind_start);
        end
        
        
        %% initialize
        yd_subset = NaN*ones(nburst,1);
        for ff = 1:length(fields)
            eps_subset.(fields{ff}) = NaN*ones(nburst,1);
            sigmaN_subset.(fields{ff}) = NaN*ones(nburst,1);
            R2_subset.(fields{ff}) = NaN*ones(nburst,1);
            switch epsoptions.method 
                case 'cdiff'
                    D_subset.(fields{ff}) = NaN*ones(nburst,nz_R0);
                case'allR'
                    npts = nchoosek(epsoptions.nrmax-1,2);
                    D_subset.(fields{ff}) = NaN*ones(nburst,npts);
            end
            Rinfo_subset.(fields{ff}).slope = NaN*ones(nburst,1);
            Rinfo_subset.(fields{ff}).yint = NaN*ones(nburst,1);
            Rinfo_subset.(fields{ff}).CI_slope = NaN*ones(nburst,2);
            Rinfo_subset.(fields{ff}).CI_yint = NaN*ones(nburst,2);
        end

        
        %% cycle and calculate epsilon
        for nn = 1:nburst
            inds_t = ind_start(nn):ind_end(nn);
            npts = length(inds_t);
            
            yd_subset(nn) = get_yd(mean(adcp.mtime(inds_t)));
            
            % reference speed
            if length(fields) == 4
                Uref = sqrt(nanmean(adcp.v1(ind_z,inds_t),2).^2 + nanmean(adcp.v2(ind_z,inds_t),2).^2+...
                    nanmean(adcp.v3(ind_z,inds_t),2).^2 + nanmean(adcp.v4(ind_z,inds_t),2).^2);
                epsoptions.Uref = Uref;
            else
                epsoptions.Uref = NaN;
            end
            
            for ff = 1:length(fields)
                 % Get primed velocity
                v = adcp.(fields{ff})(inds_zavg,inds_t);
                vm = nanmean(v,2); % mean
                vp = (v - vm*ones(1,npts))'; % fluctuation
                
%                 v = adcp.(fields{ff});
%                 vm = nanmean(v(1:epsoptions.bin_max,inds),2); % mean
%                 vp.(fields{ff}) = (v(1:epsoptions.bin_max,inds) - vm*ones(1,npts))'; % fluctuation
                
                switch epsoptions.method
                    case 'cdiff'

                        [eps_tmp,sigN_tmp,R2_tmp,D_tmp,r] = calc_eps_ADCP_SF(vp,z,epsoptions);
                        Rinfo_tmp.slope = NaN;
                        Rinfo_tmp.yint = NaN;
                        Rinfo_tmp.CI_slope = NaN;
                        Rinfo_tmp.CI_yint = NaN;
                    case 'allR'
                        epsoptions.order = 2;
                        [eps_tmp,sigN_tmp,R2_tmp,r,D_tmp,Rinfo_tmp] = calc_eps_ADCP_SF_allR(vp,z,epsoptions);
                end
                
                eps_subset.(fields{ff})(nn,:) = real(eps_tmp);
                sigmaN_subset.(fields{ff})(nn,:) = sigN_tmp;
                R2_subset.(fields{ff})(nn,:) = R2_tmp;
                D_subset.(fields{ff})(nn,:) = D_tmp;
                
                Rinfo_subset.(fields{ff}).slope(nn) = Rinfo_tmp.slope;
                Rinfo_subset.(fields{ff}).yint(nn) = Rinfo_tmp.yint;
                Rinfo_subset.(fields{ff}).CI_slope(nn,:) = Rinfo_tmp.CI_slope;
                Rinfo_subset.(fields{ff}).CI_yint(nn,:) = Rinfo_tmp.CI_yint;
                
                
                

            end

            
            disp_percdone(nn,nburst,10)
        end
        
        %% concatenate
        yd = [yd; yd_subset];
        for ff = 1:length(fields)
            eps.(fields{ff}) = [eps.(fields{ff}); eps_subset.(fields{ff})];
            sigmaN.(fields{ff}) = [sigmaN.(fields{ff}); sigmaN_subset.(fields{ff})];
            R2.(fields{ff}) = [R2.(fields{ff}); R2_subset.(fields{ff})];
            D.(fields{ff}) = [D.(fields{ff}); D_subset.(fields{ff})];

            Rinfo.(fields{ff}) = catstructs_JMM(Rinfo.(fields{ff}),Rinfo_subset.(fields{ff}),1);
        end

        
        clear adcp
    end

    %% save data
    
    if flg.savemat ==1
        disp(['Saving:', fileinfo.savefile])
    metadata = get_metadata(mname);
    save(fileinfo.savefile,'yd','z','eps','sigmaN','R2','D','r','Rinfo','epsoptions','metadata')
    end
end