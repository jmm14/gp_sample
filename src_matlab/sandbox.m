clear

load ../output/GP130620BPb_level3.mat
load ../output/GP130620BPb_level4.mat

indT = 249;
indZ = 17;
indB = 1;

r = squeeze(lev3.R_DEL(indT,indZ,:,indB));
D = squeeze(lev3.DLL(indT,indZ,:,indB));
xi = r.^(2/3);
yi = D;


[eps,sigmaN,R2,Rinfo] = calc_eps_SF(r',D',struct('order',2,'figure',1,'sigmaN_v',0.0466));
return
slopeC = lev4.REGRESSION_COEFF_A1(indT,indZ,indB);
yintC = lev4.REGRESSION_COEFF_A0(indT,indZ,indB);

figure(1),clf
plot(xi,yi,'o')
hold all

xfit = linspace(1,2.8,100);
plot(xfit,slopeC*xfit+yintC,'k')

% regression (with CIs)
X = [ones(size(xi)) xi];
[b,bint] = regress(yi,X);
slope = b(2);
yint = b(1);
d_slope = 0.5*diff(bint(2,:));
d_yint = 0.5*diff(bint(1,:));

if abs(slope-slopeC)<1e-6
    disp('Slope agrees')
end
if abs(yint-yintC)<1e-6
    disp('yint agrees')
end
    

plot(xfit,slope*xfit+yint)


plot_CI_regression_line(0.95,b,xi,yi,struct('nPts',100,'xRange',[0 3],'plotLines',1))

xlim([0,3])