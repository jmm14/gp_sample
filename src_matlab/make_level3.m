 %%
% Make structures for level 3 - i.e. Dll calculation
% 
% Justine McMillan
% 2022-01-15
%
% 2022-04-09: Script adapted to only compute level 3
% 2022-06-23: Script updated for consistency with Wiki
% 2022-06-27: Function updated to apply flags with separate function 
% 2023-04-08: Script updated to compute DLL using forward difference as
%             defined on the Wiki

%%

clear all
addpath('functions')
mname = mfilename('fullpath');
dataFile = '../output/GP130620BPb_level2.mat';
outFile  = '../output/GP130620BPb_level3.mat';
flagFile = '../metadata/RDI4beam_TidalChannel_GP130620BPb_flags.yml';
metaFile = '../metadata/RDI4beam_TidalChannel_GP130620BPb_groups.yml';


%% Load data
load(dataFile)
metadata = ReadYaml(metaFile);

%% Options

% calclulation of structure function 
optionsDLL.order = 2;
optionsDLL.diffMethod = metadata.L3.dll_method;
optionsDLL.figure = 0;



%% Sizes
NT = length(lev2.N_SEGMENT);
NZ = length(lev2.Z_DIST);
NB = length(lev2.N_BEAM);
NS = length(lev2.N_SAMPLE);
NR = length(lev2.Z_DIST)-1; 



% %% Get bin pairs to use for 'All R' method (using all r_del combinations within r_max)
% nBinMax = optionsDLL.nbinMax;
% nBinMaxHalf = floor((nBinMax+1)/2); % number of bins from centre for centered schemes
% binPairsRel = nchoosek([-nBinMaxHalf:0, 1:nBinMaxHalf],2);
% 
% % Remove 1 bin separation and bin pairs that exceed max specified separation (necessary if nbinMax is odd)
% binDiff = binPairsRel(:,2) - binPairsRel(:,1);
% indKeep = find((binDiff >= 2) & (binDiff <= optionsDLL.nbinMax));
% binPairsRel = binPairsRel(indKeep,:);
% 
% % Sort by separation (easier for viewing)
% [~,indSort] = sort(binPairsRel(:,2)-binPairsRel(:,1));
% binPairsRel = binPairsRel(indSort,:);
% 
% NR = length(binPairsRel);



%% Dimensions
lev3.TIME = nanmean(lev2.TIME,2);
lev3.Z_DIST = lev2.Z_DIST;
lev3.N_BEAM = lev2.N_BEAM;
lev3.N_DEL = [1:NZ-1]; 
lev3.N_BOUND = [1 2];

%% Time bounds
lev3.TIME_BNDS = [nanmin(lev2.TIME,[],2) nanmax(lev2.TIME,[],2)];

%% Initialize
lev3.R_DEL = NaN*ones(NB,NR); 
lev3.BIN_L = NaN*ones(NZ,NR); % My variable: Lower bin for difference
lev3.BIN_U = NaN*ones(NZ,NR); %  My variable: Upper bin for difference
lev3.DLL = NaN*ones(NT,NZ,NB,NR);
lev3.DLL_FLAGS = zeros*ones(NT,NZ,NB,NR); % Perfect data
lev3.DLL_N = NaN*ones(NT,NZ,NB,NR);
lev3.N_SEGMENT = lev2.N_SEGMENT; % Note: NOT NEEDED FOR ANALYSIS, SHOULD THIS BE OPTIONAL?
lev3.R_DIST = ((lev3.Z_DIST*ones(1,NB)) ./ cosd(ones(NZ,1)*lev2.THETA'));

%% Calculate DLL
count = 0;
disp('* Computing DLL *')
for bb = 1:NB
    dr = lev3.R_DIST(2,bb)-lev3.R_DIST(1,bb); % Assumes uniform bin separation
    lev3.R_DEL(bb,:) = lev3.N_DEL*dr;
    
    % Loop through ensembles and calculate DLL
    for tt = 1:NT
        
        % Get velocity timeseries for range bins
        vp = reshape(lev2.R_VEL_DETRENDED(tt,:,bb,:),[NZ,NS])';
        
        % Calculate velocity differences
        if strcmp(optionsDLL.diffMethod,'forward')
            [dll,dll_n,binL,binU] = calc_DLL_DLLL_forward_diff(vp,optionsDLL);
        else
            error('Not implemented for specified differencing method')
        end
        
        % Assign to structures
        lev3.DLL(tt,:,bb,:) = dll;
        lev3.DLL_N(tt,:,bb,:) = dll_n;
        lev3.N_SEGMENT(tt) = tt;
        
        % Show progress
        count = count+1;
        disp_percdone(count,NT*NB,5)
    end % tt
end % bb

% Same for every time and beam 
lev3.BIN_L = binL;
lev3.BIN_U = binU;

%% Apply flags (Need to manually ensure all flags in yaml file are here)
disp('==== Applying Flags =====')
lev3.DLL_N_ratio = lev3.DLL_N / NS;
lev3 = apply_ATOMIX_flags(lev3,'L3','DLL_FLAGS',flagFile,struct('dispOutput',1));
lev3 = rmfield(lev3,'DLL_N_ratio');
disp('==== Applying Flags (End) =====') 

%% Plot to check (Level 3 diagnostics)
indR = 5;
indB = 1;
drBins = lev3.R_DEL(indB,indR)/dr;

figure(2),clf
ax(1) = subplot(311);
pcolor(get_yd(lev3.TIME),lev3.Z_DIST,squeeze(lev3.DLL(:,:,indB,indR)'))
shading flat
caxis([0,0.02])
colorbar
title(['DLL - \delta r = ',num2str(drBins),' bins'])

ax(2) = subplot(312);
pcolor(get_yd(lev3.TIME),lev3.Z_DIST,squeeze(lev3.DLL_N(:,:,indB,indR)'))
shading flat
colorbar
title('DLL_N')

ax(3) = subplot(313);
pcolor(get_yd(lev3.TIME),lev3.Z_DIST,squeeze(lev3.DLL_FLAGS(:,:,indB,indR)'))
shading flat
colorbar
title('Flags')

%% Save
disp(['Saving:' outFile])
lev3 = orderfields_dim_first(lev3,{'TIME','Z_DIST','N_BEAM','N_DEL','N_BOUND'});
history = get_metadata(mname,history);
save(outFile,'lev3','history')