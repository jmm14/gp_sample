% Plot a comparison between the output of my original code and the level 4
% data generated for ATOMIX.
%
% Justine McMillan
% Jan 13, 2022

clear all

fileOrig10m = '../output/GP130620BPb_epsSF2_10m.mat';
fileOrigAll = '../output/GP130620BPb_epsSF2_All.mat';

dataOrig10m = load(fileOrig10m);
dataOrigAll = load(fileOrigAll);

%% Averages
dataOrig10m.eps_mean = mean([dataOrig10m.eps.v1'; dataOrig10m.eps.v2'; dataOrig10m.eps.v3'; dataOrig10m.eps.v4']);
dataOrigAll.eps_mean = zeros(length(dataOrigAll.z),length(dataOrigAll.yd));
for ii = 1 : length(dataOrigAll.z)
    dataOrigAll.eps_mean(ii,:) = mean([dataOrigAll.eps.v1(ii,:)' dataOrigAll.eps.v2(ii,:)' dataOrigAll.eps.v3(ii,:)' dataOrigAll.eps.v4(ii,:)'],2);
end
%%
figure(1),clf
plot(dataOrig10m.yd,dataOrig10m.eps_mean)
hold all
plot(dataOrigAll.yd,dataOrigAll.eps_mean(17,:))
title('\epsilon at 10 m')

%%
figure(2),clf
pcolor(dataOrigAll.yd,dataOrigAll.z,log10(dataOrigAll.eps_mean))
shading flat
set(gca,'Ydir','norm')
colorbar

%% Compare reformatted data
load ../output/GP130620BPb_level4.mat
figure(3),clf
subplot(211)
pcolor(dataOrigAll.yd,dataOrigAll.z,log10(dataOrigAll.eps_mean))
shading flat
colorbar
caxis([-6 -2])
xlim(get_yd([lev4.TIME(1) lev4.TIME(end)]))
title('original')

subplot(212)
pcolor(get_yd(lev4.TIME),lev4.Z_DIST,log10(lev4.EPSI_FINAL'))
shading flat
colorbar
caxis([-6 -2])
title('new')

%%
figure(4),clf
plot(dataOrig10m.yd,dataOrig10m.eps_mean)
hold all
plot(dataOrigAll.yd,dataOrigAll.eps_mean(17,:))
plot(get_yd(lev4.TIME),lev4.EPSI_FINAL(:,17))
title('\epsilon at 10 m')
xlim(get_yd([lev4.TIME(1) lev4.TIME(end)]))
legend('10m','All','level4')

