% Plot raw data for a segment
%
% Justine McMillan
% 2022-01-14

clear
load ../output/GP130620BPb_level1.mat
load ../output/GP130620BPb_level2.mat
load ../output/GP130620BPb_level3.mat
load ../output/GP130620BPb_level4.mat
load ../output/GP130620BPb_vel_data.mat

indT = 30%110; % Time segment to extract

indTime = remove_nans(lev2.N_PROFILE(indT,:));

%% Examples

% Velocities
figure(1),clf
subplot(211)
plot(lev1.TIME,lev1.R_VEL(:,20,3))
hold all
plot(lev1.TIME(indTime),lev1.R_VEL(indTime,20,3))
subplot(212)
indS = find(~isnan(lev2.TIME(indT,:)));
plot(lev1.TIME(indTime),lev1.R_VEL(indTime,20,3))
hold all
plot(lev2.TIME(indT,:),squeeze(lev2.R_VEL(indT,20,:,3)))
legend('Raw (Lev1)','QC (Lev2)')
title('Segment')
%% Plot segment and check detrending
figure(10),clf
plot(lev1.TIME(indTime),lev1.R_VEL(indTime,20,3))
hold all
plot(lev2.TIME(indT,:),squeeze(lev2.R_VEL(indT,20,:,3)))
plot(lev2.TIME(indT,:),squeeze(lev2.R_VEL_DETRENDED(indT,20,:,3)))
plot(lev2.TIME(indT,:),squeeze(lev2.R_VEL(indT,20,:,3))-squeeze(lev2.R_VEL_DETRENDED(indT,20,:,3)))
plot(get(gca,'xlim'),[0 0],'k')
legend('Raw','QC','Detrended','Trend (vRaw - vDetrend)')
ylabel('[m/s]')
%indS = find(~isnan(lev2.TIME(indT,:)));
%plot(lev1.TIME(indTime),lev1.R_VEL(indTime,20,3)-squeeze(lev2.R_VEL(indT,20,indS,3)))
%%
% Pitch
figure(2),
plot(lev1.TIME(indTime),lev1.PITCH(indTime))

% Bad data
indZ = 40;
figure(3),clf
subplot(211)
plot(lev1.TIME(indTime),lev1.CORR(indTime,indZ,3))
hold all
plot(get(gca,'xlim'),100*[1 1],'k')

subplot(212)
plot(lev1.TIME(indTime),lev1.R_VEL_FLAGS(indTime,indZ,3),'o')

%% 
figure(4),clf,set(gcf,'color','white')
subplot(211)
pcolor(get_yd(velAvg.TIME),velAvg.Z_DIST,velAvg.velSigned')
shading flat
colorbar
title('signed speed')
subplot(212)
pcolor(get_yd(lev4.TIME),lev4.Z_DIST,log10(lev4.EPSI_FINAL'))
shading flat
colorbar
title('\epsilon')

figure(40),clf,set(gcf,'color','white')
subplot(211)
plot(get_yd(velAvg.TIME),velAvg.velSigned(:,17))
title('signed speed - z = 10 m')
subplot(212)
semilogy(get_yd(lev4.TIME),lev4.EPSI_FINAL(:,17))
shading flat
grid on
title('\epsilon - z = 10 m')
ylim([5e-6 5e-4])

%% Get data for an epsilon value
indTall = [35 70 97];
indZ = 6;
indB = 3;
figure(5),clf
for ii = 1:length(indTall)
    subplot(length(indTall)+1,1,ii)
    plot(lev2.TIME(indTall(ii),:)-lev1.TIME(1),squeeze(lev2.R_VEL_DETRENDED(indTall(ii),indZ,:,indB)))
    title(['\epsilon = ',num2str(lev4.EPSI(indTall(ii),indZ,indB)),' W/kg'])
    ylim([-0.3 0.3])
    grid on
end
subplot(length(indTall)+1,1,ii+1)
pcolor(1:length(lev4.TIME),lev4.Z_DIST,log10(lev4.EPSI_FINAL'))
shading flat
colorbar
hold all
plot([indTall(1) indTall(1)],get(gca,'xlim'),'k')
plot([indTall(2) indTall(2)],get(gca,'xlim'),'k')
plot([indTall(3) indTall(3)],get(gca,'xlim'),'k')
plot(get(gca,'xlim'),[indZ indZ],'k')