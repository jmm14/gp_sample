% Plots the structure function (i.e. Dll vs r^(2/3)) for two segments of
% data and shows the linear fit. See also 'plot_SF_fit_func.m' for a
% function that plots the fits with confidence intervals.
%
% Justine McMillan
% Feb 9, 2022

clear

load ../output/GP130620BPb_level3.mat
load ../output/GP130620BPb_level4.mat

ydHigh = 175.5; % Time when dissipation is high
ydLow = 175.6; % Time when dissipation is low

%% Get indices
indTAll = [get_nearestindex(get_yd(lev4.TIME),ydHigh),get_nearestindex(get_yd(lev4.TIME),ydLow)];
%indZ = 17; % Good data near 10 m 
indZ = 38; % Poor data near the surface
indB = 1; % Beam index

%% Get r and D values and apply flags
rAll = lev3.R_DEL;
DAll = lev3.DLL;
rQC  = rAll;
rQC(lev3.DLL_FLAGS>0) = NaN; % WIll only be different if flags havent been applied
DQC = DAll;
DQC(lev3.DLL_FLAGS>0) = NaN;

%%
figure(1),clf
for tt = 1:length(indTAll)
    indT = indTAll(tt);
    
    rNow = squeeze(rAll(indT,indZ,:,indB));
    DNow = squeeze(DAll(indT,indZ,:,indB));
    rQCNow = squeeze(rQC(indT,indZ,:,indB));
    DQCNow = squeeze(DQC(indT,indZ,:,indB));
    
    % info about flags
    epsi = lev4.EPSI(indT,indZ,indB);
    epsi_flag = lev4.EPSI_FLAGS(indT,indZ,indB);
    DLL_flags = lev3.DLL_FLAGS(indT,indZ,:,indB);
    disp('--')
    disp(['indT = ', num2str(indT),', epsi = ',num2str(epsi)])
    disp_flags(epsi_flag,struct('fileName','../metadata/GP130620BPb_atomix_flags_attributes.yml','levelName','L4','varName','EPSI_FLAGS'))
%     for dd = 1:length(DLL_flags)
%         if DLL_flags(dd)~=0
%             disp(num2str(rNow(dd)^(3/2)))
%             disp_flags(DLL_flags(dd),struct('fileName','metadata/GP130620BPb_atomix_flags.yml','levelName','Level3','varName','DLL_FLAGS'))
%         end
%     end
            
    
    legendstr{tt} = ['\epsilon = ', num2str(epsi,'%3.2e'),' W/kg, ',...
                     'N = ',num2str(lev4.REGRESSION_N(indT,indZ,indB)), ', ',...
                     'EPSI\_FLAG = ',num2str(epsi_flag)];
    p(tt) = plot(rQCNow.^(2/3),DQCNow,'o','LineWidth',2); hold all
    plot(rNow.^(2/3),DNow,'.','markersize',8);
    xval=[1 2.8];
    
    % Regresstion lines and 95% CI
    beta = [lev4.REGRESSION_COEFF_A0(indT,indZ,indB),lev4.REGRESSION_COEFF_A1(indT,indZ,indB)];
    [yUpp,yLow,xFit,yFit] = plot_CI_regression_line(0.95,beta,rQCNow.^(2/3),DQCNow,struct('nPts',100,'xRange',[0 3],'plotLines',0));
    plot(xFit,yFit,'k','linewidth',2)
    plot(xFit,yLow,'--','color',0.4*[1 1 1],'linewidth',1)
    plot(xFit,yUpp,'--','color',0.4*[1 1 1],'linewidth',1)
end    
legend(p,legendstr)
titlestr = ['z = ',num2str(lev4.Z_DIST(indZ)),' m, ',...
            'b = ',num2str(lev4.N_BEAM(indB))];
title(titlestr)
xlabel('r^{2/3} [m^{2/3}]')
ylabel('D_{LL} [m^2 s^{-2}]')