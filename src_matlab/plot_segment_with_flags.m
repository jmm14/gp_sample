% Plot a specific segment of the raw data and show the flags.
% Also a check to ensure that level 2 segmented data is equal to level 1
% data.
%
% Justine McMillan
% Feb 9, 2022
clear

load ../output/GP130620BPb_level1.mat
load ../output/GP130620BPb_level2.mat

indT = 10;
indZ = 43;
indB = 1;

%% Get data

% Level 1
t1 = lev1.TIME;
v1 = squeeze(lev1.R_VEL(:,indZ,indB));
v1f = squeeze(lev1.R_VEL_FLAGS(:,indZ,indB));
c1 = squeeze(lev1.CORR(:,indZ,indB));

% Level 2
t2 = lev2.TIME(indT,:);
v2 = squeeze(lev2.R_VEL(indT,indZ,:,indB));
%v2f = squeeze(lev2.R_VEL(indT,indZ,:,indB));
%% Plot
figure(1),clf
ax(1) = subplot(3,1,1);
plot(get_yd(t1),v1,'.-')
hold all
plot(get_yd(t2),v2,'.-')
title(['indZ = ',num2str(indZ),', indT = ',num2str(indT),', indB = ',num2str(indB)])
legend('Level1','Level2')
ylabel('R\_VEL')

ax(2) = subplot(3,1,2);
plot(get_yd(t1),v1f,'.-')
ylabel('R\_VEL\_FLAGS')

ax(3) = subplot(3,1,3);
plot(get_yd(t1),c1,'.-')
hold all 
plot(get(gca,'xlim'),[100 100]) % Current threshold
ylabel('CORR')

linkaxes(ax,'x')
xlim([min(get_yd(t2))-5/1440 max(get_yd(t2))+5/1440]) % Segment plus/minus 5 minutes
xlabel('yd 2013')