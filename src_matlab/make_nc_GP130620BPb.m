%%
% make_nc_GP130620BPb
%
% Justine McMillan
% 2022-01-05
% 
% JMM, 2022-06-20: Update to also export .mat file
%%

clear all
mname = mfilename('fullpath');
%% File names
fileInfo.ncFile = '../output/RDI4beam_TidalChannel_GP130620BPb_JMM_20231229.nc';
fileInfo.matFile = '../output/RDI4beam_TidalChannel_GP130620BPb_JMM_20231229.mat';
fileInfo.flagFile = '../metadata/RDI4beam_TidalChannel_GP130620BPb_flags.yml';
fileInfo.metaFileGlobal = '../metadata/RDI4beam_TidalChannel_GP130620BPb_global.yml';
fileInfo.metaFileGroup = '../metadata/RDI4beam_TidalChannel_GP130620BPb_groups.yml';

if exist(fileInfo.ncFile, 'file') == 2
    disp([fileInfo.ncFile ' already exists and will be overwritten. Hit any key to continue.']),pause
end

%% MetaData
metadataGlobal = ReadYaml(fileInfo.metaFileGlobal,[],1);
metadataGroups = ReadYaml(fileInfo.metaFileGroup,[],1);
metadataGroups.L1.time_ref = datestr(metadataGroups.L1.time_ref,'yyyy-mm-dd HH:MM:SS'); % Hack to convert reference time to string (otherwise, I get error when creating netcdf)
timeRef = datenum(metadataGroups.L1.time_ref); % Reference time needed to create netcdf file

%% Data
% Level 1
data1 = load('../output/GP130620BPb_level1.mat');
data.L1 = data1.lev1;

% Level 2
data2 = load('../output/GP130620BPb_level2.mat');
data.L2 = data2.lev2;

% Level 3
data3 = load('../output/GP130620BPb_level3.mat');
data.L3 = data3.lev3;

% Level 4
data4 = load('../output/GP130620BPb_level4.mat');
data.L4 = data4.lev4;

%% Ancillary Velocity data
dataVel = load('../output/GP130620BPb_vel_data.mat');
if max(abs(dataVel.velAvg.TIME - data.L4.TIME))>1e-6
    error('TIME values for L4 and velocity data are not the same')
else
    data.Ancillary.TIME = data.L4.TIME;
    data.Ancillary.TIME_BNDS = data.L4.TIME_BNDS;
end
data.Ancillary.Z_DIST = data.L4.Z_DIST;
data.Ancillary.N_BEAM = data.L4.N_BEAM;
data.Ancillary.N_BOUND = [1 2]';
data.Ancillary.ENU = NaN*ones(length(dataVel.velAvg.TIME),length(dataVel.velAvg.Z_DIST),4);
data.Ancillary.ENU(:,:,1) = dataVel.velAvg.velEast;
data.Ancillary.ENU(:,:,2) = dataVel.velAvg.velVert;
data.Ancillary.ENU(:,:,3) = dataVel.velAvg.velVert;
data.Ancillary.ENU(:,:,4) = dataVel.velAvg.velError;
data.Ancillary.SIGNED_SPEED = dataVel.velAvg.velSigned;
data.Ancillary = orderfields_dim_first(data.Ancillary,{'TIME','Z_DIST','N_BEAM','N_BOUND'});


%% Flags
flags = ReadYaml(fileInfo.flagFile);

%% Make matfile
disp('===== CREATE MAT FILE =====')

metadataGlobal = metadataGlobal;
history = get_metadata(mname,cat(1,data4.history',dataVel.history));
save(fileInfo.matFile,'data','flags','metadataGroups','metadataGlobal','fileInfo','history');

disp(['Matfile created: ' fileInfo.matFile])

disp('===== CREATE MAT FILE (end) =====')

%% Create netcdf file
disp('===== CREATE NETCDF FILE =====')
% % Remove cells
% data.L4 = rmfield(data.L4,'REGRESSION_DLL');
% data.L4 = rmfield(data.L4,'REGRESSION_R_DEL');
% data.L4 = rmfield(data.L4,'REGRESSION_IND_DLL');

% Add flags and metadata to data
vF=fieldnames(flags);
gF=fieldnames(data);
mF=fieldnames(metadataGroups);
nGrp=length(gF); % nbre Groups
for ii=1:nGrp
    if any(strcmp(gF{ii},vF))
        data.(gF{ii}).Flags=flags.(gF{ii});
    end
    
    if any(strcmp(gF{ii},mF))
        data.(gF{ii}).Meta=metadataGroups.(gF{ii});
    end
   
end

% Create netcdf file
create_netcdf_fielddata(data,metadataGlobal,fileInfo.ncFile,1,timeRef);
[~,oname]=fileparts(fileInfo.ncFile);
movefile(strcat(oname,'.nc'),fileInfo.ncFile)

disp(['Netcdf created: ' fileInfo.ncFile])

disp('===== CREATE NETCDF FILE (end) =====')