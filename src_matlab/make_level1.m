%%
% Make structure for level1
%
% Justine McMillan
% 2022-01-05
%%

clear all
addpath('functions')
mname = mfilename('fullpath');

%% Data files
dataFile = '../output/GP130620BPb_RawData.mat';
outFile = '../output/GP130620BPb_level1.mat';
flagFile = '../metadata/RDI4beam_TidalChannel_GP130620BPb_flags.yml';

%% Load Data
load(dataFile)

%% Dimensions
lev1.TIME = dataRaw.mtime;
lev1.Z_DIST = dataRaw.hab; % Along beam bin center difference FIX?
lev1.N_BEAM = [1:1:dataRaw.n_beams]';
NT = length(lev1.TIME);
NZ = length(lev1.Z_DIST);
NB = dataRaw.n_beams;

%% Data
lev1.PROFILE_NUMBER = [1:NT]';
lev1.R_VEL = dataRaw.vel;
lev1.THETA = dataRaw.theta*ones(NB,1); 
lev1.BIN_SIZE = (dataRaw.hab(2)-dataRaw.hab(1))*ones(NB,1);
lev1.ABSI = dataRaw.amp;
lev1.CORR = dataRaw.cor;
lev1.HEADING = dataRaw.heading;
lev1.PITCH = dataRaw.pitch;
lev1.ROLL = dataRaw.roll;
lev1.PRES = dataRaw.pressure;
lev1.TEMP = dataRaw.temperature;

%% Apply flags
disp('==== Applying Flags =====')
lev1.R_VEL_FLAGS = zeros(NT,NZ,NB); % Perfect data
lev1 = apply_ATOMIX_flags(lev1,'L1','R_VEL_FLAGS',flagFile,struct('dispOutput',1));

disp('==== Applying Flags (end) =====')

%% Save
lev1 = orderfields_dim_first(lev1,{'TIME','Z_DIST','N_BEAM'});
history = get_metadata(mname,history);
disp(['Saving:' outFile])
save(outFile,'lev1','history')