%% 
% Make structure for all velocities including ENU and signed speed. 
% Not required by ATOMIX so keep separate.
%
% TODO: Apply quality control to these data based on lev1 flags. (use sum
% from all beams?)
%
% Justine McMillan 
% Mar 3, 2022

clear
mname = mfilename('fullpath');
dataFile = '../output/GP130620BPb_level1.mat';
outFile = '../output/GP130620BPb_vel_data.mat';
%%
load(dataFile)

%% Get structure in desired format
adcp.v1 = lev1.R_VEL(:,:,1)';
adcp.v2 = lev1.R_VEL(:,:,2)';
adcp.v3 = lev1.R_VEL(:,:,3)';
adcp.v4 = lev1.R_VEL(:,:,4)';
adcp.heading = lev1.HEADING;
adcp.pitch = lev1.PITCH;
adcp.roll = lev1.ROLL;

cfg.beam_pattern = 'convex';
cfg.beam_angle = 20;

%% Time and Range
velRaw.TIME = lev1.TIME;
velRaw.Z_DIST = lev1.Z_DIST;

%% ENU Velocities
[velRaw.velEast,velRaw.velNorth,velRaw.velVert,velRaw.velError,...
            velRaw.velX,velRaw.velY,velRaw.velZ] = rdi_coordTransformFINAL(adcp,cfg);

%% Signed speed and direction
velRaw.velDirMagN = get_DirFromN(velRaw.velEast,velRaw.velNorth);
velRaw.velSigned = sqrt(velRaw.velEast.^2+velRaw.velNorth.^2);
velRaw.velSigned = sign_speed(velRaw.velEast,velRaw.velNorth,velRaw.velSigned,velRaw.velDirMagN,0);


%% Calculate averages
ensLength = 300; % [s]
freqApprox = 1.5; % [Hz]

% Calculate Ensemble indices
[ind_start, ind_end] = get_ens_inds(lev1.TIME*24*3600,ensLength,freqApprox); 
NT = length(ind_start);
NZ = length(lev1.Z_DIST);

% Initialize
fields = {'velEast','velNorth','velVert','velError','velX','velY','velZ','velDirMagN','velSigned'};
velAvg.TIME = NaN*ones(NT,1);
velAvg.Z_DIST = velRaw.Z_DIST;
for ff = 1:length(fields)
    field = fields{ff};
    velAvg.(field) = NaN*ones(NZ,NT); 
end

% Calculate averages
for nn = 1:NT
    nPtsSegment = ind_end(nn) - ind_start(nn) + 1;
    velAvg.TIME(nn) = mean(lev1.TIME(ind_start(nn):ind_end(nn)));
    
    for ff = 1:length(fields)
        field = fields{ff};
        velAvg.(field)(:,nn) = nanmean(velRaw.(field)(:,ind_start(nn):ind_end(nn)),2);  
    end
end

%% Transpose fields (to have same shape as ATOMIX format)
fields = {'velEast','velNorth','velVert','velError','velX','velY','velZ','velDirMagN','velSigned'};
for ff = 1:length(fields)
    velRaw.(fields{ff}) = velRaw.(fields{ff})';
    velAvg.(fields{ff}) = velAvg.(fields{ff})';
end

%% Save
history = get_metadata(mname);
save(outFile,'velRaw','velAvg','history')