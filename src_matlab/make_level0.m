%%
% Extract raw data for one day and put in simple structure
% 
% Justine McMillan
% 2022-01-05
%%

clear
mname = mfilename('fullpath');
site = 'GP130620BPb';
fileinfo.datadir = '../../../Data/GP130620BPb/RawData/';
fileinfo.fileroot = 'BPb_';
limT = [datenum(2013,6,23,23,57,30) datenum(2013,6,24,23,57,30)];
limZ = 27; % m
habTransducer = 0.5; % m
declination = -17.18; % Based on experiment date and location

%% Load ancillary data
datafile = [fileinfo.datadir fileinfo.fileroot 'AncData.mat'];

load(datafile)

% vertical bins
dataRaw.hab = adcp.config.ranges +habTransducer;
indZ = find(dataRaw.hab < limZ);
dataRaw.hab = dataRaw.hab(indZ);

% time
indT = find(adcp.mtime>=limT(1) & adcp.mtime<limT(2));
dataRaw.mtime = adcp.mtime(indT)';

% orientation
dataRaw.heading = adcp.heading(indT)'+declination;
dataRaw.pitch = adcp.pitch(indT)';
dataRaw.roll = adcp.roll(indT)';

% pressure/temp
dataRaw.pressure = adcp.pressure(indT)';
dataRaw.temperature = adcp.temperature(indT)';

% beams
dataRaw.n_beams = adcp.config.n_beams;
dataRaw.theta = adcp.config.beam_angle_too;

% plot
figure(2),clf
fields = {'pressure','heading','pitch','roll'};
for ff = 1:4
    field = fields{ff};
    subplot(4,1,ff)
    plot(adcp.mtime,adcp.(field))
    hold all
    plot(dataRaw.mtime,dataRaw.(field))
end

%% Load beam data
dataRaw.vel = zeros(length(dataRaw.mtime),length(dataRaw.hab),4);
dataRaw.amp = zeros(length(dataRaw.mtime),length(dataRaw.hab),4);
dataRaw.cor = zeros(length(dataRaw.mtime),length(dataRaw.hab),4);
for bb = 1:4
    datafile = [fileinfo.datadir fileinfo.fileroot 'beam' num2str(bb) '_wk1.mat'];
    disp(datafile)
    load(datafile)
    
    indT = find(adcp.mtime>limT(1) & adcp.mtime<limT(2));

    mtime_tmp = adcp.mtime(indT)';
    
    if mtime_tmp ~= dataRaw.mtime
        warning('Time vectors not equal')
    end
    dataRaw.vel(:,:,bb) = adcp.(['v',num2str(bb)])(indZ,indT)';
    dataRaw.cor(:,:,bb) = adcp.(['c',num2str(bb)])(indZ,indT)';
    dataRaw.amp(:,:,bb) = adcp.(['A',num2str(bb)])(indZ,indT)';
    
end

%% Save
history = get_metadata(mname);
save(['../output/',site,'_RawData'],'dataRaw','history')

%% Plot
figure(1),clf
plot(dataRaw.mtime,dataRaw.pressure)