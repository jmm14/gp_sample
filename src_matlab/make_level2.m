%%
% Make structure for level2
%
% Justine McMillan
% 2022-01-14
%%

clear all
mname = mfilename('fullpath');
dataFile = '../output/GP130620BPb_level1.mat';
outFile = '../output/GP130620BPb_level2.mat';

%% Parameters
ensLength = 300; % [s]
freqApprox = 1.5; % [Hz]
detrendMethod = 'Regress1D'; % Method to detrend segments

%% Load Data
load(dataFile)

%% Calculate Ensembles
disp('* GETTING ENSEMBLES *')
[ind_start, ind_end] = get_ens_inds(lev1.TIME*24*3600,ensLength,freqApprox); 
disp('=====')

%% Sizes
NS = max(ind_end-ind_start)+1; % Number of points in an ensemble
NT = length(ind_end);
NZ = length(lev1.Z_DIST);
NB = length(lev1.N_BEAM);


%% Dimensions
lev2.N_SEGMENT = [1:NT]';
lev2.N_SAMPLE = [1:NS]';
lev2.Z_DIST = lev1.Z_DIST; 
lev2.N_BEAM = lev1.N_BEAM;

%% Data

% Initialize
lev2.TIME = NaN*ones(NT,NS);
%lev2.R_VEL = NaN*ones(NT,NZ,NS,NB);
lev2.R_VEL_DETRENDED = NaN*ones(NT,NZ,NB,NS);
lev2.PROFILE_NUMBER = NaN*ones(NT,NS); % Optional variable for data point number to make it easy to go back to level 1 data

% Segment
disp('* SEGMENTING *')
for nn = 1:NT
    nPtsSegment = ind_end(nn) - ind_start(nn) + 1;
    
    lev2.TIME(nn,1:nPtsSegment) = lev1.TIME(ind_start(nn):ind_end(nn));
    lev2.PROFILE_NUMBER(nn,1:nPtsSegment) = ind_start(nn):ind_end(nn);

    % QC and detrend
    for bb = 1:NB
        % Get data
        velB = lev1.R_VEL(ind_start(nn):ind_end(nn),: ,bb);
        velBFlags = lev1.R_VEL_FLAGS(ind_start(nn):ind_end(nn),:,bb);
        
        % Check that dimensions
        if sum(size(velB)~=[nPtsSegment,NZ]) > 0 
            velB = velB';
            velBFlags = velBFlags';
            disp('Transposing so columns are Z dimension')
        end
        
        % Create mask and apply QC
        velBFlags(velBFlags>0) = NaN;
        velBFlags = velBFlags+1;
        velBqc = velB.*velBFlags;
        
        % Detrend (Transpose to detrend time series for each depth)
         velBdt = detrend_with_NaNs(velBqc,lev2.TIME(nn,1:nPtsSegment),struct('method',detrendMethod));
%         velBdt = detrend_with_NaNs(velBqc,[],struct('method',detrendMethod,'showOutput',0));
        
        % Assign
        %lev2.R_VEL(nn,:,1:nPtsSegment,bb) = velBqc;
        lev2.R_VEL_DETRENDED(nn,:,bb,1:nPtsSegment) = velBdt';
    end

    disp_percdone(nn,NT,10)
end
disp('===')
 
%% Plot to check

if 0
    figure(1),clf
    np = 120;
    for nn = 1:np
        subplot(1,np,nn)
    %     plot(24*(lev2.TIME(nn,:)-lev1.TIME(1)),squeeze(lev2.R_VEL(nn,20,:,1)))
        pcolor(24*(lev2.TIME(nn,:)-lev1.TIME(1)),lev2.Z_DIST,squeeze(lev2.R_VEL(nn,:,:,3))); shading flat
        caxis([-1 1])
        ylim([0 30])
        set(gca,'yticklabel',[]);
    end
end

%% Other variables
lev2.THETA = lev1.THETA;
lev2.BIN_SIZE = lev1.BIN_SIZE;
%lev2.R_DIST = lev1.R_DIST;

%% Save
disp(['Saving:' outFile])
lev2 = orderfields_dim_first(lev2,{'N_SEGMENT','Z_DIST','N_BEAM','N_SAMPLE'});
history = get_metadata(mname,history);
save(outFile,'lev2','history')