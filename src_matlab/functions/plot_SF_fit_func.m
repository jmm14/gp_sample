function plot_SF_fit_func(lev3,lev4,options)

if ~isfield(options,'indT'); options.indT = 1; end
if ~isfield(options,'indZ'); options.indZ = 1; end
if ~isfield(options,'indB'); options.indB = 1:4; end



nPlots = length(options.indB);


figure(111),clf
set(gcf,'Name','SFfit')

for pp = 1:nPlots
    
    
    indT = options.indT;
    indZ = options.indZ;
    indB = options.indB(pp);
    
    % Data
    r = squeeze(lev3.R_DEL(indT,indZ,:,indB));
    D = squeeze(lev3.DLL(indT,indZ,:,indB));
    epsi = lev4.EPSI(indT,indZ,indB);
    epsiFlag = lev4.EPSI_FLAGS(indT,indZ,indB);
    A0 = lev4.REGRESSION_COEFF_A0(indT,indZ,indB);
    A1 = lev4.REGRESSION_COEFF_A1(indT,indZ,indB);
    R2 = lev4.REGRESSION_R2(indT,indZ,indB);
    epsiDelRatio = lev4.EPSI_DEL_RATIO(indT,indZ,indB);
    
    % QC data
    rQC = r;
    rQC(lev3.DLL_FLAGS(indT,indZ,:,indB)>0) = NaN; % WIll only be different if flags havent been applied
    DQC = D;
    DQC(lev3.DLL_FLAGS(indT,indZ,:,indB)>0) = NaN;
    
    % Plot
    ax(pp) = subplot(1,nPlots,pp); lstr = {};
    plot(r.^(2/3),D,'.')
    lstr{end+1} = 'All Data';
    hold all
    plot(rQC.^(2/3),DQC,'o')
    lstr{end+1} = 'QC Data';
    

     
    % Regresstion lines and 95% CI
    beta = [A0,A1];
    [yUpp,yLow,xFit,yFit] = plot_CI_regression_line(0.95,beta,r.^(2/3),D,struct('nPts',100,'xRange',[0 3],'plotLines',0));
    plot(xFit,yFit,'k','linewidth',2)
    lstr{end+1} = 'Fit';
    plot(xFit,yLow,'--g','linewidth',2)
    lstr{end+1} = 'CI';
    plot(xFit,yUpp,'--g','linewidth',2)


    
    legend(lstr)
    
    titlestr = {['Beam',num2str(indB),', \epsilon = ',num2str(epsi,'%3.1e'),' W/kg'],...
                 ['EPSI\_FLAG = ',num2str(epsiFlag)],...
                 ['\Delta \epsilon/\epsilon = ',num2str(epsiDelRatio,'%4.2f'),', R^2 = ',num2str(R2,'%3.2f')]};
    title(titlestr)
end

ylabel(ax(1),'DLL [m^2/s^2]')
for pp = 1:nPlots
    xlabel(ax(pp),'r^{2/3} [m^{2/3}]')
end


