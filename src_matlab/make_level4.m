 %%
% Make structures for 4 -- i.e. dissipation calculation
% 
% Justine McMillan
% 2022-01-15
%
% 2022-04-09: Script adapted to only compute level 4
% 2022-06-20: Update for consistency of variables with Wiki
% 2022-06-26: Update to use separate function to apply flags
% 2023-04-08: Update to use forward difference DLL for all methods

%%

clear all
addpath('functions')
mname = mfilename('fullpath');
dataFile = '../output/GP130620BPb_level3.mat';
outFile  = '../output/GP130620BPb_level4.mat';
flagFile = '../metadata/RDI4beam_TidalChannel_GP130620BPb_flags.yml';
metaFile = '../metadata/RDI4beam_TidalChannel_GP130620BPb_groups.yml';


%% Load data
load(dataFile)
metadata = ReadYaml(metaFile);

%% Processing Options

optionsEps.Const = metadata.L4.C2;
optionsEps.order = 2;
optionsEps.rMin = metadata.L4.rMin;
optionsEps.rMax = metadata.L4.rMax;
optionsEps.points_select_method = metadata.L4.points_select_method;
optionsEps.dll_averaging = str2num(metadata.L4.dll_averaging);
optionsEps.figure = 0;

   
%% Sizes
NT = length(lev3.N_SEGMENT);
NZ = length(lev3.Z_DIST);
NB = length(lev3.N_BEAM);




%% Dimensions
lev4.TIME = lev3.TIME;
lev4.Z_DIST = lev3.Z_DIST;
lev4.N_BEAM = lev3.N_BEAM;
lev4.N_FIT = NaN; % To be set below
lev4.N_BOUND = [1 2]';

%% Time bounds
lev4.TIME_BNDS = lev3.TIME_BNDS;

%% Initialize
lev4.EPSI = NaN*ones(NT,NZ,NB);
lev4.EPSI_FINAL = NaN*ones(NT,NZ);
lev4.C2 = optionsEps.Const; % Note: Should this just be in the yml file?
lev4.EPSI_FLAGS = zeros(NT,NZ,NB); % Perfect data
lev4.EPSI_CI_HIGH = NaN*ones(NT,NZ,NB);
lev4.EPSI_CI_LOW = NaN*ones(NT,NZ,NB);
lev4.EPSI_DEL_RATIO = NaN*ones(NT,NZ,NB); % MY METRIC
lev4.R_MAX = NaN*ones(NT,NZ,NB);
lev4.REGRESSION_COEFF_A0 = NaN*ones(NT,NZ,NB);
lev4.REGRESSION_COEFF_A1 = NaN*ones(NT,NZ,NB);
lev4.REGRESSION_R2 = NaN*ones(NT,NZ,NB);
lev4.REGRESSION_N = NaN*ones(NT,NZ,NB);
REGRESSION_DLL = cell(NT,NZ,NB);
REGRESSION_R_DEL = cell(NT,NZ,NB);
REGRESSION_IND_DLL = cell(NT,NZ,NB);

%% Calculate epsilon
count = 0;
binL = lev3.BIN_L;
binU = lev3.BIN_U;
for bb = 1:NB
    rDel = squeeze(lev3.R_DEL(bb,:));
    dr = rDel(2) - rDel(1); % Assumes uniform separation
    
    nBinMin = floor(optionsEps.rMin./dr); % Min number of bins to use
    nBinMax = floor(optionsEps.rMax./dr); % Max number of bins to use
    
    
    % Loop through ensembles and calculate DLL and epsilon
    for tt = 1:NT
        % Get simple variables
        dll = squeeze(lev3.DLL(tt,:,bb,:));
        dll_flags = squeeze(lev3.DLL_FLAGS(tt,:,bb,:));
        
        % Create mask and apply QC
        dll_flags(dll_flags>0) = NaN;
        dll_flags = dll_flags+1;
        dllQC = dll.*dll_flags;
        
        for zz = 1:NZ
            
            % Get points for fit (depends on the method)
            [rDelFit,dllFit,rDelAll,dllAll,indDll,binPairs] = get_DLL_fit_data(zz,rDel,dllQC,binL,binU,dr,optionsEps);
           
            % Linear regression to get epsilon
            if length(rDelFit)>0
                [epsi,sigmaN,R2,Rinfo] = calc_eps_SF(rDelFit,dllFit,optionsEps);
                
                % Assign to structures
                lev4.EPSI(tt,zz,bb) = epsi;
                lev4.R_MAX(tt,zz,bb) = max(rDelFit);
                lev4.REGRESSION_COEFF_A0(tt,zz,bb) = Rinfo.yint;
                lev4.REGRESSION_COEFF_A1(tt,zz,bb) = Rinfo.slope;
                lev4.REGRESSION_N(tt,zz,bb) = Rinfo.npts;
                lev4.REGRESSION_R2(tt,zz,bb) = R2;
                lev4.EPSI_CI_LOW(tt,zz,bb) = real((Rinfo.CI_slope(1)/optionsEps.Const)^(3/2));
                lev4.EPSI_CI_HIGH(tt,zz,bb) = real((Rinfo.CI_slope(2)/optionsEps.Const)^(3/2));
                lev4.EPSI_DEL_RATIO(tt,zz,bb) = Rinfo.d_eps/epsi; % MY METRIC
                REGRESSION_R_DEL{tt,zz,bb} = rDelFit;
                REGRESSION_DLL{tt,zz,bb} = dllFit;
                REGRESSION_IND_DLL{tt,zz,bb} = indDll;
            end
            
            count = count+1;
            disp_percdone(count,NT*NB*NZ,5)
        end
    end
end

%% Convert cells to matrices
len = cellfun(@length,REGRESSION_DLL);
NR = max(len(:));

[NT,NZ,NB] = size(lev4.EPSI);
lev4.REGRESSION_DLL = NaN*ones(NT,NZ,NB,NR);
lev4.REGRESSION_R_DEL = NaN*ones(NT,NZ,NB,NR);

lev4.N_FIT = [1:NR]';
for tt = 1:NT
    for zz = 1:NZ
        for bb=1:NB
            nr = length(REGRESSION_DLL{tt,zz,bb});
            lev4.REGRESSION_DLL(tt,zz,bb,1:nr) = REGRESSION_DLL{tt,zz,bb};
            lev4.REGRESSION_R_DEL(tt,zz,bb,1:nr) = REGRESSION_R_DEL{tt,zz,bb};
        end
    end
end

%% Apply flags (Need to manually ensure all flags in yaml file are here)
disp('==== Applying Flags =====')
lev4 = apply_ATOMIX_flags(lev4,'L4','EPSI_FLAGS',flagFile,struct('dispOutput',1));
disp('==== Applying Flags (End) =====')


%% Calculate final epsilon
epsitmp = lev4.EPSI;
epsitmp(lev4.EPSI_FLAGS>0) = NaN;

lev4.EPSI_FINAL = nanmean(epsitmp,3);


%% Plots to check (Level 4 diagnostics)
figure(2),clf
ax(1) = subplot(511);
pcolor(lev4.TIME,lev4.Z_DIST,log10(lev4.EPSI(:,:,1)'))
shading flat
colorbar
title('\epsilon')

ax(2) = subplot(512);
pcolor(lev4.TIME,lev4.Z_DIST,squeeze(lev4.REGRESSION_N(:,:,1)'))
shading flat
colorbar
title('REGRESSION\_N')

ax(3) = subplot(513);
pcolor(lev4.TIME,lev4.Z_DIST,squeeze(lev4.REGRESSION_R2(:,:,1)'))
shading flat
colorbar
title('REGRESSION\_R2')

ax(4) = subplot(514);
pcolor(lev4.TIME,lev4.Z_DIST,squeeze(lev4.EPSI_DEL_RATIO(:,:,1))')
shading flat
colorbar
caxis([0 1])
title('\Delta\epsilon/\epsilon')

ax(5) = subplot(515);
pcolor(lev4.TIME,lev4.Z_DIST,squeeze(lev4.EPSI_FLAGS(:,:,1)'))
shading flat
colorbar
title('FLAGS')

% Same as above, but for one bin
indZ = 17;
figure(3),clf
ax(1) = subplot(611);
plot(get_yd(lev4.TIME),log10(lev4.EPSI(:,indZ,1)'))
hold all
plot(get_yd(lev4.TIME),log10(lev4.EPSI_CI_LOW(:,indZ,1)'),'--')
plot(get_yd(lev4.TIME),log10(lev4.EPSI_CI_HIGH(:,indZ,1)'),'--')
title('\epsilon')

ax(2) = subplot(612);
plot(get_yd(lev4.TIME),squeeze(lev4.REGRESSION_N(:,indZ,1)'))
hold all
flagInfo = get_flag_info(flagFile,'L4','EPSI_FLAGS','regression_poorly_conditioned',struct());
plot(get(gca,'xlim'),[1 1]*flagInfo.flag_thresholds,'--')
title('REGRESSION\_N')

ax(3) = subplot(613);
plot(get_yd(lev4.TIME),squeeze(lev4.REGRESSION_COEFF_A0(:,indZ,1)'))
hold all
flagInfo = get_flag_info(flagFile,'L4','EPSI_FLAGS','dll_intercept_too_low',struct());
plot(get(gca,'xlim'),[1 1]*flagInfo.flag_thresholds,'--')
flagInfo = get_flag_info(flagFile,'L4','EPSI_FLAGS','dll_intercept_too_high',struct());
plot(get(gca,'xlim'),[1 1]*flagInfo.flag_thresholds,'--')
title('REGRESSION\_A0')

ax(4) = subplot(614);
plot(get_yd(lev4.TIME),squeeze(lev4.REGRESSION_R2(:,indZ,1)'))
hold all
flagInfo = get_flag_info(flagFile,'L4','EPSI_FLAGS','Rsquared_too_low',struct());
plot(get(gca,'xlim'),[1 1]*flagInfo.flag_thresholds,'--')
title('REGRESSION\_R2')

ax(5) = subplot(615);
plot(get_yd(lev4.TIME),squeeze(lev4.EPSI_DEL_RATIO(:,indZ,1))')
hold all
flagInfo = get_flag_info(flagFile,'L4','EPSI_FLAGS','delta_epsi_too_large',struct());
plot(get(gca,'xlim'),[1 1]*flagInfo.flag_thresholds,'--')
ylim([0 5])
title('\Delta\epsilon/\epsilon')

ax(6) = subplot(616);
plot(get_yd(lev4.TIME),squeeze(lev4.EPSI_FLAGS(:,indZ,1)'))
title('FLAGS')

grid on
linkaxes(ax,'x')

%% Plot final epsilon
figure(4),clf, clear ax
ax(1) = subplot(5,1,1);
pcolor(lev4.TIME,lev4.Z_DIST,log10(lev4.EPSI_FINAL(:,:)'))
shading flat
colorbar
caxis([-5 -2])
title('\epsilon\_final')

for ii = 1:4
    ax(ii) = subplot(5,1,ii+1);
    pcolor(lev4.TIME,lev4.Z_DIST,log10(lev4.EPSI(:,:,ii)'))
    shading flat
    colorbar
    caxis([-5 -2])
    title(['\epsilon_',num2str(ii)])
end


%% Save
disp(['Saving:' outFile])
lev4 = orderfields_dim_first(lev4,{'TIME','Z_DIST','N_BEAM','N_FIT','N_BOUND'});
history = get_metadata(mname,history);
save(outFile,'lev4','history')
