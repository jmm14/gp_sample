Files used in the preparation of Grand Passage data for ATOMIX testing.

Process to create netcdf file using **Matlab code** (src_matlab):

1. make_level0.m : Extracts required data for one day

2. make_level1.m: Put level1 data in structure (i.e. raw data with required names)

3. make_level2.m: Put level2 data in structure

4. make_level3_level4.m: Put level 3 and level 4 data in structure

5. make_vel_struct.m: Creates a structure with velocity data in ENU and XYZ coordinates, which are included as ancillary data

5. make_nc_GP130620BPb: Read in the structures and use Cynthia's tools to create a .mat file and a netcdf. 



Process to create netcdf file using **Python code**: TBD

**Metadata and flag** information set in yml files in 'metadata' directory.

**Dependant upon the following repositories**

* https://bitbucket.org/jmm14/adcp_toolbox : Functions for processing ADCP data

* https://bitbucket.org/jmm14/netcdftools_ceb : Cynthia's netcdf toolbox

* https://bitbucket.org/jmm14/utilitieswork : General functions for miscelleneous tasks

Last updated: Jul 8, 2022
